<!--

    @description: Menu de usuário que ainda não fez login

 -->

<?php
    include('_includes.php');
?>

<div class='login-box'>
    <h1>Fazer Login</h1>
    <form action="php/login.php" method="post">

        <div class="form-group">
            <label for="userLogin"><i class="far fa-user"></i></label>
            <input type="text"      name="userLogin" id="userLogin" placeholder="Usuário">
        </div>

        <div class="form-group">
            <label for="userPwd"><i class="fas fa-at"></i></label>
            <input type="password"  name="userPwd"   id="userPwd"   placeholder="Senha">
        </div>

        <div class="form-group">
            <button type="submit" name="btnLogin" id="btnLogin" class="btn btn-form-login">Login</button>
        </div>

    </form>
    <span class="login-information">
        Caso não saiba seu usuário e senha entre em contato com a T.I.

    </span>


    <div class="restric-area">
    <span>
            Links Uteis
            <div class="util-link links">
                <ul>
                    <li>
                        <a href="http://177.69.21.131" target="_blank">Site Antigo</a>
                    </li>                    <li>
                        <a href="http://suporte.unimedam.com.br" target="_blank">GLPI</a>
                    </li>
                    <li>
                        <a href="http://gmail.unimedaltamogiana.com.br" target="_blank">G-mail</a>
                    </li>
                    <li>
                        <a href="http://pep.unimedam.com.br:8400/mksaude/site" target="_blank">MK-Saúde</a>
                    </li>
                    <li>
                        <a href="http://totvs.unimedam.com.br:8080/menu-html" target="_blank">Totvs</a>
                    </li>
                    <li>
                        <a href="https://www.netsabe.com.br/" target="_blank">Lista Telefonica</a>
                    </li>
                    <li>
                        <a href="http://www.unimedfesp.coop.br/" target="_blank">FESP</a>
                    </li>
                    <li>
                        <a href="https://www.unimed.coop.br/" target="_blank">Unimed Brasil</a>
                    </li>
                    <li>
                        <a href="http://www.ufenesp.com.br/" target="_blank">Nordeste Paulista</a>
                    </li>
					<li>
                        <a href="http://intranet.unimedam.com.br/intranet/downloads/TVQS.exe" target="_blank">TeamViewer QS</a>
                    </li>
                </ul>
            </div>
        </span>
    </div>

    <div class="aux-button">
        <div class="connect-ti btn-rounded">
           <a href="downloads/Unisuporte.exe">
               <img src="assets/imgs/suport.png" alt="UniSuporte" title="UniSuporte">
           </a>

        </div>
    </div>
</div>