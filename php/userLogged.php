<!--

    @description: Menu de usuário que já esta logado 

 -->
 <?php
    include('_includes.php');
    hasSession();
 ?>

<div class='login-box' id="login-box">
    <div class="user-welcome">
        Bem vindo <span class="user-welcome-name"> <?php echo getName($_SESSION['nome']); ?> </span>
        <span class="logout"><a href="php/logout.php">Sair</a></span>
    </div>

    <div class="restric-area">
        <span>
            Area Restrita
            <div class="restric-link links">
                <ul>
                    <li>
                        <a href="#">Arquivos</a>
                    </li>
                    <li>
                        <a href="#">Salute</a>
                    </li>

                    <!-- Libera link painel de admin -->
                    <?php
                        if($_SESSION['user_type'] == 'A'){
                            echo "<li>".
                                    "<a href='view/admin/'>Admin Panel </a>".
                                 "</li>";
                        }
                    ?>

                </ul>
            </div>
        </span>

         <span>
            Links Uteis
            <div class="util-link links">
                <ul>
                    <li>
                        <a href="http://177.69.21.131" target="_blank">Site Antigo</a>
                    </li>                    <li>
                        <a href="http://suporte.unimedam.com.br" target="_blank">GLPI</a>
                    </li>
                    <li>
                        <a href="http://gmail.unimedaltamogiana.com.br" target="_blank">G-mail</a>
                    </li>
                    <li>
                        <a href="http://pep.unimedam.com.br:8400/mksaude/site" target="_blank">MK-Saúde</a>
                    </li>
                    <li>
                        <a href="http://totvs.unimedam.com.br:8080/menu-html" target="_blank">Totvs</a>
                    </li>
                    <li>
                        <a href="https://www.netsabe.com.br/" target="_blank">Lista Telefonica</a>
                    </li>
                    <li>
                        <a href="http://www.unimedfesp.coop.br/" target="_blank">FESP</a>
                    </li>
                    <li>
                        <a href="https://www.unimed.coop.br/" target="_blank">Unimed Brasil</a>
                    </li>
                    <li>
                        <a href="http://www.ufenesp.com.br/" target="_blank">Nordeste Paulista</a>
                    </li>
                </ul>
            </div>
        </span>
    </div>

    <div class="aux-button">
        <div class="point-watch">
            <a href="view/point/" class="btn-rounded">
                <img src="assets/imgs/clock.png" alt="Ponto Virtual" title="Ponto Virtual">
            </a>
        </div>

        <div class="connect-ti ">
            <a href="downloads/Unisuporte.exe" class="btn-rounded">
                <img src="assets/imgs/suport.png" alt="UniSuporte" title="UniSuporte">
            </a>

        </div>
    </div>
</div>