<?php
    // @description: Arquivo que contem funções de utilização global pelo sistema.
     
    // Retorna o primeiro nome do funcionario informado, recomendado
    // receber o nome atraves da coluna FUNNOME da tabela USUARIO.
    function getName($name){
        return explode(' ', $name)[0];
    }

    // Verifica se a sessão esta iniciada.
    function hasSession(){
        session_start();
        if(!isset($_SESSION['user'])){
            Header("Location: http://intranet.unimedam.com.br/intranet/?errn=1");
            die();
        }
    }

?>