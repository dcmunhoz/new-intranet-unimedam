<?php
    // @description: Rotina para insersão de um novo usuário.

    // Includes
    include_once('_includes.php');
    
    // Inicia variavel de sessão
    session_start();


    // Verifica a sessão
    hasSession();

    // Verifica se o usuário tem permissão de admin
    if($_SESSION['user_type'] != 'A'){
        Header('Location: ../../?errn=1');
    }

    $myCon  = new databaseConnect('intranet');
    $myLink = $myCon->startMysql(); 

    // Variaveis com dados para insersão
    $loginUser       = $_POST['loginUser'];
    $nameUser        = $_POST['nameUser'];
    $pisUser         = $_POST['pisUser'];
    $pwdUser         = $_POST['pwdUser'];
    $emailUser       = $_POST['emailUser'];
    $slTypeUser         = $_POST['slTypeUser'];
    if($_POST['chkPermiteVirtual'] == 'true'){
        $chkPermiteVirtual  = 'S';
    }else{
        $chkPermiteVirtual = 'N';
    }

    $query = "INSERT INTO usuarios(login, senha, nome, pisnum, email, permite_virtual, user_type) values('$loginUser', md5('$pwdUser'), '$nameUser', '$pisUser', '$emailUser', '$chkPermiteVirtual', '$slTypeUser');";

    if($rq = mysqli_query($myLink, $query)){
        mysqli_commit($myLink);
        echo 'Usuário Inserido com sucesso.';
    }else{
        die('Erro: ' . mysqli_error($myLink));
    }