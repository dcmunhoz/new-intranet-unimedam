<!--

    @description: Rotina de login do usuário

 -->

<?php 

    // Inclue o arquivo de includes na pagina.
    include('_includes.php');

    // 1 - Verifica se existe na variavel post os campos de usuário e senha
    // 2 - Verifica se as variaveis não estão vazias.
    if( ( !isset($_POST['userLogin']) && !($_POST['userLogin'] != '') ) && ( !isset($_POST['userPwd']) && !($_POST['userPwd'] != '') ) ){
        // Caso não esteja criado ou seja vazio, retorna a pagina inicial com erro.
        Header('Location: ../?errn=1');
    }

    // Instância o objeto de banco passando como parametro o nome da aplicação que deseja conectar. 
    $conIntranet = new databaseConnect('intranet');
    $conZeus     = new databaseConnect('zeus');

    // Recebe o link de conexão, pode somente receber a conexão com pase na aplicação passada por parametro.
    // EX: Saude - SqlServer, GLPI - MySQL, Zeus - Firebird
    $linkIntranet   = $conIntranet->startMysql();
    $linkZeus       = $conZeus->startFirebird();

    // Captura o usuário e senha
    $userLogin  = addslashes($_POST['userLogin']);
    $userPwd    = addslashes($_POST['userPwd']);

    // Query buscando usuário e senha passados.
    $query      = "SELECT * FROM usuarios WHERE login = '$userLogin' and senha = md5('$userPwd');";

    // Executa a query
    if($resultado_query = mysqli_query($linkIntranet, $query)){
        // Insere a quantidade de linhas atingidas na query executada em uma variavel.
        $rowsNum = mysqli_num_rows($resultado_query);

        // Se o numero de linhas for maio que 0 o usuário existe.
        if($rowsNum > 0){
            if($rowData = mysqli_fetch_array($resultado_query)){
                // Verifica se o usuário esta ativo
                if($rowData['ativo'] == 'S'){
                    // Inicia as variaveis de sessão
                    session_start();
                    $_SESSION['user']        = $rowData['login'];           // Login do usuário
                    $_SESSION['nome']        = $rowData['nome'];            // Nome usuário
                    $_SESSION['pisnum']      = $rowData['pisnum'];          // Numero do PIS
                    $_SESSION['perm_mark']   = $rowData['permite_virtual']; // Parametro para verificar se é permitido marcar ponto.
                    $_SESSION['user_type']   = $rowData['user_type'];       // Tipo do usuario ADMIN/USER
                    
                    // Query para pegar outros dados necessários e guarda-los na superglobal SESSION
                    $GetFunData = "SELECT * FROM funcionario WHERE funpis = '". $rowData['pisnum'] ."' AND funativo = 'True';";

                    // Executa a query
                    if($rqfd = ibase_query($linkZeus, $GetFunData)){
                        $funData = ibase_fetch_assoc($rqfd);
                        
                        $_SESSION['empcodigo'] = $funData['EMPCODIGO'];     // Codigo da empresa
                        $_SESSION['fundesdoc'] = $funData['FUNDES_DOCTO'];  // Documento do funcionario
                        Header('Location: ../');
                    }else{
                        die("Error: " . ibase_errmsg());
                    }
                }else{
                    Header('Location: ../?errn=5');
                }
            }
            
        }else{
            Header('Location: ../?errn=2');
        }
    }else{
        die('Error: ' . mysqli_error($linkIntranet));
    }