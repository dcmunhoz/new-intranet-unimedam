<?php
    
    // @description: Rotina para fazer a conexão com os bancos de dados.

    // *** É necessário passar como parametro na hora de instanciar esta classe
    // *** o nome da aplicação que deseja fazer a conexão. Apos isso, é necessário criar uma
    // *** variavel passando como link a conexão com o banco desejado.
    // *** Obs: Só pode ser feito uma conexão com o banco compativel com a da aplicação informada.

    // *** Zeus - Firebird, Intranet - MySQL, Saude - SQL Server

    Class databaseConnect{

        // Propriedades
        private $base   = ''; // Banco utilizado pela aplicação
        private $sistem = ''; // Nome Sistema
        private $host   = ''; // Servidor banco
        private $user   = ''; // Usuário banco
        private $pass   = ''; // Senha banco
        private $dbName = ''; // Nome banco
        private $port   = ''; // Porta de conexão

        // Construtor
        function __construct($sistem){
            // Verifica qual o sistema informado e configura as pripriedades com base na conexão para o banco
            switch($sistem){
                case 'intranet':
                    $this->base     = 'MySQL';
                    $this->sistem   = $sistem;
                    $this->host     = '192.168.130.10';
                    $this->user     = 'intranet';
                    $this->pass     = 'intranet';
                    $this->port     = '3306';
                    $this->dbName   = 'intranet';
                    break;
                case 'zeus':
                    $this->base     = 'Firebird';
                    $this->sistem   = $sistem;
                    $this->host     = '192.168.130.21:c:\\zeus\\ponto.gdb';
                    $this->user     = 'sysdba';
                    $this->pass     = 'masterkey';
                    break;
            }   
        }

        // Inicia a conexão com MySql
        function startMysql(){
            if(!($this->base == 'MySQL')){
                die('Erro: System databse is not MySQL.');
            }

            $con = mysqli_connect($this->host, $this->user, $this->pass, $this->dbName, $this->port);
                
            if(!$con){
                die('Error: '. mysqli_connect_error($con));
            }
            return $con;
        }

        // Inicia a conexão com o Firebird
        function startFirebird(){
            if(!($this->base == 'Firebird')){
                die('Erro: System databse is not Firebird.');
            }

            $con = ibase_connect($this->host, $this->user, $this->pass, 'UTF-8');
            if(!$con){
                die('Error: ' . ibase_errmsg());
            }
            
            return $con;
        }

    }