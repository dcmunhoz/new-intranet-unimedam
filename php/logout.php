<?php

    // @description: Rotina para finalizar a sessão do usuário

    include('_includes.php');
    hasSession();

    // Destroi as variaveis de sessão criadas
    unset($_SESSION['user']);
    unset($_SESSION['nome']);
    unset($_SESSION['pisnum']);
    unset($_SESSION['perm_mark']);
    unset($_SESSION['empcodigo']);
    unset($_SESSION['fundesdoc']);

    Header('Location: ../');
