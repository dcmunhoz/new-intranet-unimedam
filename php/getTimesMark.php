<?php

    // @description: Rotina para pegar as marcações virtuais do dia feitas pelo usuário logado 

    // Inclui o arquivo de includes.
    include('_includes.php');
    // Inicia a sessão no arquivo.
    hasSession();

    // Instância o objeto de conexão com banco.
    $conZeus    = new databaseConnect('zeus');
    $linkZeus   = $conZeus->startFirebird(); 

    // Pega o numero do documento do funcionario cadastrado.
    $fundesdoc  = $_SESSION['fundesdoc'];
    // Dia da buca a ser realizada. Variavel passada atraves do javascript.
    $diaBusca   = $_POST['diaBusca'];

    // Query para selecioanr os horários marcados.
    $query = "SELECT mvrhora FROM MOVRELOGIOVIRTUAL where FUNDES_DOCTO = '$fundesdoc' and MVRDATA = '$diaBusca';";

    // Executa a query passando o resultado para uma variavel
    // que recebera o escopo de tabela.
    $data = '';
    if($rq = ibase_query($linkZeus, $query)){
        while($row = ibase_fetch_assoc($rq)){
            $data .= "<tr> \n";
            $data .= "  <td> \n";
            $data .= "      ".$row['MVRHORA'] . " \n";
            $data .= "  </td> \n";
            $data .= "</tr> \n";
        }
    }else{
        die('Erro: ' . ibase_errmsg());
    }

    // Exibe o conteudo da variavel para ser capturada via javascript utilizando Ajax
    echo $data;
    
?>