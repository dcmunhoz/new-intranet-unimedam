<?php 

    // @description: Rotina para realizar as marcações do ponto.

    include('_includes.php');
    hasSession();

    $zeusCon  = new databaseConnect('zeus');
    $zeusLink = $zeusCon->startFirebird(); 

    // Pega os dados que são necessários para realizar a marcação.
    $selectedPisFun = $_POST['selectedPisFun']; // Pis do funcionario.
    $userEmpCodigo  = $_SESSION['empcodigo'];   // Codigo da empresa.
    $userFunDesDoc  = $_SESSION['fundesdoc'];   // Documento do funcionario.
    $day            = $_POST['fullDay'];        // Dia da marcação
    $hour           = $_POST['fullHour'];       // Horário da marcação

    // Verifica se o funcionario tem permição para fazer a marcação
    if($_SESSION['perm_mark'] == 'S'){ // Se sim, executa a rotina.
        
        // Query que insere na tabela os dados necessários para efetivar a marcação
        $query_insert = "INSERT INTO MOVRELOGIOVIRTUAL(EMPCODIGO, FUNDES_DOCTO, MVRDATA, MVRHORA, MVRCODIGORELOGIO, MVRORIGEM) VALUES('$userEmpCodigo', '$userFunDesDoc', '$day', '$hour', 'Virt', 'O');";
        // Executa a query
        if($rqi = ibase_query($zeusLink, $query_insert)){
            echo 'true';
        }else{
            die('Erro: ' . ibase_errmsg());
        }  
    }else{  // Se não, retorna uma mensagem de erro.
        echo 'Você não tem permição, favor solicitar ao RH';
    }

?>