<?php

    // @description: Arquivo que centraliza todos os request e includes do sistema.

    include_once('db/db.class.php');    // Script conexão com banco de dados.
    include_once('_functions.php');     // Script contendo funções globais.