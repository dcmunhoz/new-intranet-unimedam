<?php
    // Includes
    include_once('_includes.php');
    
    // Inicia variavel de sessão
    session_start();


    // Verifica a sessão
    hasSession();

    // Verifica se o usuário tem permissão para acessar o painel de admin
    if($_SESSION['user_type'] != 'A'){
        Header('Location: ../../?errn=1');
    }

    $myCon  = new databaseConnect('intranet');
    $myLink = $myCon->startMysql(); 


    // Varial com o valor do checkbox
    $check = $_POST['check'];
    $active = '';
    if($check == 'true'){
        $active = 'S';
    }else{
        $active = '%';
    }

    $query = "SELECT * FROM usuarios WHERE ativo LIKE '$active';";

    if($rq = mysqli_query($myLink, $query)){
        while($row = mysqli_fetch_array($rq)){
            switch($row['ativo']){
                case 'S':
                    $situacao = "Ativo";
                    break;
                default:
                    $situacao = "Inativo";
                    break;
            }

            switch($row['user_type']){
                case 'A':
                    $userType = "Administrador";
                    break;
                default:
                    $userType = "Usuário";
                    break;
            }

            switch($row['permite_virtual']){
                case 'S';
                    $permVirt = "Sim";
                    break;
                default:
                    $permVirt = "Não";
                    break;
            }

            if($row['ativo'] == 'S'){
                $classInactive = "";
                $btnType = "<button type='button' name='ul-btnInactive-".$row['id']."' id='ul-btnInactive".$row['id']."' class='btn-ul ul-inactive' data-action='N' data-iduser='".$row['id']."'><i class='fas fa-user-times'></i></button>";
            }else{
                $classInactive = "class='inactive'";
                $btnType = "<button type='button' name='ul-btnActive-".$row['id']."'  id='ul-btnActive".$row['id']."'  class='btn-ul ul-active' data-action='S' data-iduser='".$row['id']."'><i class='fas fa-user-check'></i></button>";
            }


            echo "<tr $classInactive>".
                    " <input type='hidden' id='pis-user-".$row['id']."' value='".$row['pisnum']."'>".
                    " <input type='hidden' id='virtual-user-".$row['id']."' value='".$row['permite_virtual']."'>".
                    "<td id='login-user-".$row['id']."'>".$row['login']."</td>".
                    "<td id='name-user-".$row['id']."'>".utf8_encode($row['nome'])."</td>".
                    "<td id='status-user-".$row['id']."'>".$situacao."</td>".
                    "<td id='type-user-".$row['id']."'>".$userType."</td>".
                    "<td> ".$permVirt." </td>".
                    //data-userId=".$row['id']."
                    "<td class='ul-btnOption' ><button type='button' name='ul-btnEdit".$row['id']."' id='ul-btnEdit".$row['id']."' class='ul-edit' data-action='edit' data-iduser='".$row['id']."'><i class='fas fa-user-edit'></i></button>$btnType</td>".
                 "</tr>";
        }
    }else{
        die('ERRO: ' . mysqli_error($myLink));
    }
    
    