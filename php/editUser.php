<?php
    // @description: Rotina para edição de um usuário existente.

    // Includes
    include_once('_includes.php');
    
    // Inicia variavel de sessão
    session_start();


    // Verifica a sessão
    hasSession();

    // Verifica se o usuário tem permissão de admin
    if($_SESSION['user_type'] != 'A'){
        Header('Location: ../../?errn=1');
    }

    $myCon  = new databaseConnect('intranet');
    $myLink = $myCon->startMysql(); 

    // Variaveis com dados para edição
    $loginUser       = $_POST['loginUser'];
    $nameUser        = $_POST['nameUser'];
    $pisUser         = $_POST['pisUser'];
    $emailUser       = $_POST['emailUser'];
    $slTypeUser         = $_POST['slTypeUser'];
    if($_POST['chkPermiteVirtual'] == 'true'){
        $chkPermiteVirtual  = 'S';
    }else{
        $chkPermiteVirtual = 'N';
    }

    if($_POST['pwdUser'] != ""){
        $passConf = ", senha = md5('".$_POST['pwdUser']."')";
    }

    if($email == ""){
        $email = null;
    }

    $query = "UPDATE usuarios SET login = '$loginUser', nome = '$nameUser', pisnum = '$pisUser', email = '$email', permite_virtual = '$chkPermiteVirtual', user_type = '$slTypeUser' $passConf where pisnum = '$pisUser';";

    if($rq = mysqli_query($myLink, $query)){
        mysqli_commit($myLink);
        echo 'true';
    }else{
        echo 'Erro' . mysqli_error($myLink);
    }