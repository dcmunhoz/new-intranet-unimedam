// @description: Script pagina administração de usuários

// Exibe os usuários
function getUsuarios(){
    var check = document.getElementById('chk-filter').checked;
    $.ajax({
        method: 'POST',
        url: '../../php/getUsers.php',
        data: {check: check},
        success: function(data){
            document.getElementById('users-list').innerHTML = data;

            $('.btn-ul').click(function(){
                changeUserStatus(this);
            });

            $('.ul-edit').click(function(){
                showEditUser(this);
            });
        }
    });
}

// Ativar ou inativar usuário
function changeUserStatus(u){
    swal({
        title: "Atenção",
        text: "Deseja mudar o status do usuário?",
        buttons: true,
        dangerMode: true
    }).then((val) => {
        if(val){
            var action = u.dataset.action;
            var userId = u.dataset.iduser;
            $.ajax({
                method: 'POST',
                url:    '../../php/changeUserStatus.php',
                data: {
                    action: action,
                    userId: userId
                },
                success: function(data){
                    if(data == 'true'){
                        swal("Sucesso", "O usuário foi alterado.", "success");
                        getUsuarios();
                    }else{
                        swal("Ops....", data, "warning");
                    }
                }
            });
        }
    });


}

// Exibir painel de edição do usuário
function showEditUser(u){
    var idUserEdit      = u.dataset.iduser;
    var loginUserEdit   = document.getElementById('login-user-'+idUserEdit).innerHTML; 
    var nameUserEdit    = document.getElementById('name-user-'+idUserEdit).innerHTML; 
    var pisUserEdit     = document.getElementById('pis-user-'+idUserEdit).value; 
    // var typeUserEdit    = document.getElementById('type-user-'+idUserEdit).innerHTML;
    var virtualUserEdit = document.getElementById('virtual-user-'+idUserEdit).value;
    var typeUserEdit    = "";

    if(document.getElementById('type-user-'+idUserEdit).innerHTML == "Administrador"){
        typeUserEdit = "A";
    }else{
        typeUserEdit = "U";
    }

console.log(nameUserEdit);
    swal({
        title: "Atenção",
        text: "Deseja Altar o usuário " + loginUserEdit + " ?",
        icon: "warning",
        buttons: true
    }).then((val) => {
        if(val){
            userAction = "edit";
            document.getElementById('adm-panel').style.display = 'none';
            document.getElementById('adm-user').style.display = 'flex';

            document.getElementById('loginUser').value = loginUserEdit;
            document.getElementById('nameUser').value = nameUserEdit;
            document.getElementById('pisUser').value = pisUserEdit;
            document.getElementById('pwdUser').value = "";
            document.getElementById('cpwdUser').value = "";
            document.getElementById('emailUser').value = "";
            document.getElementById('slTypeUser').value = typeUserEdit;
            if(virtualUserEdit == 'S'){
                document.getElementById('chkPermiteVirtual').checked = true;
            }else{
                document.getElementById('chkPermiteVirtual').checked = false;
            }
        }
    });
}

// Verificar digito PIS
function digPis(pisNum){
    var pis     = pisNum.replace(".", "").replace(".", "").replace("/", "");
    var aPis    = pis.split('');
    var aMulti  = new Array(3,2,9,8,7,6,5,4,3,2); 
    var sumDig  = 0;
    var dv      = 0;

    for(var i = 0; i < aPis.length - 1; i++){
        sumDig += aPis[i] * aMulti[i];
    }

    dv = 11 - (sumDig % 11);
    if(dv >= 10){
        dv = 0;
    }


    if(dv == aPis[aPis.length - 1]){
        return true;
    }else{
        return false;
    }
}


// Inserir novos usuários
function endUser(){
    var url = "";
    if(userAction == 'insert'){
        url = 'insertUser.php';
    }else{
        url = 'editUser.php';
    }

    var isNull;
    var eqPass;
    var loginUser        = document.getElementById('loginUser').value;
    var nameUser         = document.getElementById('nameUser').value;
    var pisUser          = document.getElementById('pisUser').value;
    var pwdUser          = document.getElementById('pwdUser').value;
    var cpwdUser         = document.getElementById('cpwdUser').value;
    var emailUser        = document.getElementById('emailUser').value;
    var slTypeUser          = document.getElementById('slTypeUser').value;
    var chkPermiteVirtual   = document.getElementById('chkPermiteVirtual').checked;

    if(loginUser == null || loginUser == ''){
        isNull = true;
        document.getElementById('loginUser').classList.add('input-error');
    }else{
        document.getElementById('loginUser').classList.remove('input-error');
    }

    if(nameUser == null || nameUser == ''){
        isNull = true;
        document.getElementById('nameUser').classList.add('input-error');
    }else{
        document.getElementById('nameUser').classList.remove('input-error');
    }

    if(pisUser == null || pisUser == '' ){
        isNull = true;
        document.getElementById('pisUser').classList.add('input-error');
    }else{
        document.getElementById('pisUser').classList.remove('input-error');
    }

    if(userAction == 'insert' || (userAction == 'edit' && pwdUser != "" && cpwdUser != "")){
        if(pwdUser == null || pwdUser == ''){
            isNull = true;
            document.getElementById('pwdUser').classList.add('input-error');
        }else{
            document.getElementById('pwdUser').classList.remove('input-error');
        }
    
        if(cpwdUser == null || cpwdUser == ''){
            isNull == true;
            document.getElementById('cpwdUser').classList.add('input-error');
        }else{
            document.getElementById('cpwdUser').classList.remove('input-error');
        }

        if(pwdUser.length <= 4){
            swal("A sanha deve ter pelo menos 4 caracteres");
            return false;
        }
    }
    
    
    if(isNull){
        swal("Alguns campos precisam ser preenchidos!");
        return false;
    }

    if(pisUser.length < 14 || (pisUser.indexOf('.') == -1 && pisUser.indexOf('/') == -1)){
        swal("O Formato do pis esta incorreto!");
        document.getElementById('pisUser').classList.add('input-error');
        return false;
    }else{
        document.getElementById('pisUser').classList.remove('input-error');
    }

    if(!digPis(pisUser)){
        swal("Digito PIS não é valido");
        return false;
        document.getElementById('pisUser').classList.add('input-error');
    }else{
        document.getElementById('pisUser').classList.remove('input-error');
    }

    if(pwdUser != cpwdUser){
        swal("As senhas são conferem");
        return false;
        document.getElementById('pwdUser').classList.add('input-error');
        document.getElementById('cpwdUser').classList.add('input-error');
    }else{
        document.getElementById('pwdUser').classList.remove('input-error');
        document.getElementById('cpwdUser').classList.remove('input-error');
    }   



    $.ajax({
        method: 'POST',
        url: '../../php/' + url,
        data:{
            loginUser:       loginUser,
            nameUser:        nameUser,
            pisUser:         pisUser,
            pwdUser:         pwdUser,
            emailUser:       emailUser,
            slTypeUser:         slTypeUser,
            chkPermiteVirtual:  chkPermiteVirtual

        },
        success: function(data){
            if(data == 'true'){
                swal("Sucesso", "Gerenciamento finalizado com sucesso !", "success");
            }else{
                swal("Ops...", data, "warning");
            }
            getUsuarios();
            resetFields();
            document.getElementById('adm-panel').style.display      = 'block';
            document.getElementById('adm-user').style.display    = 'none';
    
        }
    });
}

// Reinicia os campos de usuários
function resetFields(){
    userAction       = "";
    loginUser        = document.getElementById('loginUser').value = "";
    nameUser         = document.getElementById('nameUser').value = "";
    pisUser          = document.getElementById('pisUser').value = "";
    pwdUser          = document.getElementById('pwdUser').value = "";
    cpwdUser         = document.getElementById('cpwdUser').value = "";
    emailUser        = document.getElementById('emailUser').value = "";
    document.getElementById('slTypeUser').value = 'U';
    document.getElementById('chkPermiteVirtual').checked = false;
}


$(document).ready(function(){
    getUsuarios();

    $('#chk-filter').change(function(){
        getUsuarios();
    });

    document.getElementById('btnUser').addEventListener('click', function(){
        userAction = 'insert';
        document.getElementById('adm-panel').style.display      = 'none';
        document.getElementById('adm-user').style.display    = 'flex';
    });

    document.getElementById('btnFinalizar').addEventListener('click', function(){
        endUser();
    });

    document.getElementById('btnCancelar').addEventListener('click', function(){
        resetFields();
        document.getElementById('adm-panel').style.display      = 'block';
        document.getElementById('adm-user').style.display    = 'none';
    });
});