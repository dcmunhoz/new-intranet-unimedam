// @description: Arquivo contendo toda a programação em javascript da Intranet.

// Global Vars
var axisY = 0;
var userAction = "";

// Funções Globais

// Adiciona o 0 a esquerda nas datas.
function addZero(n){
    var number = n;
    if( number < 10){
        number = '0' + number;
    }

    return number;
}

// Pega a data do dia no formato para inserir no DB. 
function getData(){
    var data            = new Date();
    var d               = addZero(data.getDate());
    var m               = addZero(data.getMonth() + 1);
    var y               = data.getFullYear();
 
    return y + '/' + m + '/' + d + ' 00:00:00';
}

// Pega a hora da marcação no formato para inserir no DB
function getHour(){
    var data            = new Date();
    var h               = addZero(data.getHours());
    var m               = addZero(data.getMinutes());
 
    return h + ":" + m;
}

// Exibir notificações
function showNotification(titulo, mensagem, icone){
    swal(titulo, mensagem, icone);
}


// Captura o texto dos erros enviados via get
function getErroMsg(){

    try{
        var url     = document.location.toString();
        var msg     = "";
        var errn    = url.split("?")[1].split("=")[1].replace("#", "").toString();

        switch(errn){
            case '1':
                msg = "Você não tem permissão para acessar esta pagina !";
                break;
            case '2':
                msg = "Usuário e(ou) senha invalido(s)!";
                break;
        }
        
        swal("Ops....", msg, "error");

    }catch(e){
        
    }
}





// Menu toggler
document.getElementById('menu-toggler-show').addEventListener('click', function(){
    document.getElementById('user-menu-collapsed').classList.add('menu-active');
    document.getElementById('dark-screen').style.opacity = '.5';
    document.getElementById('dark-screen').style.pointerEvents = 'all';
}); 

document.getElementById('menu-toggler-close').addEventListener('click', function(){
    document.getElementById('user-menu-collapsed').classList.remove('menu-active');
    document.getElementById('dark-screen').style.opacity = '0';
    document.getElementById('dark-screen').style.pointerEvents = 'none';
    
});
document.getElementById('dark-screen').addEventListener('click', function(){
    document.getElementById('user-menu-collapsed').classList.remove('menu-active');
    document.getElementById('dark-screen').style.opacity = '0';
    document.getElementById('dark-screen').style.pointerEvents = 'none';
});


// Scroll Effects
window.addEventListener('scroll', function(){
    // Get page position
    axisY = window.pageYOffset.toString();

    // Change navbar
    if(axisY >= 100){
        document.getElementById('navbar').classList.add('change-nav');
    }else{
        document.getElementById('navbar').classList.remove('change-nav');
    }

    // Background paralax
    document.getElementById('bg-img').style.backgroundPositionY = -(axisY * .4) + 'px';

});


//TESTES NOTIFICAÇÕES
// Window Load
window.onload = function(){
    getErroMsg();
}

document.getElementById('teste').addEventListener('click', function(){
    
    swal("Ops....");
});