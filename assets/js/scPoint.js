// @description: Scripts pagina de marcação de ponto

// Functions 

// Adiciona o 0 a esquerda nas datas.
function addZero(n){
    var number = n;
    if( number < 10){
        number = '0' + number;
    }

    return number;
}

// Pega a data do dia no formato para inserir no DB. 
function getData(){
    var data            = new Date();
    var d               = addZero(data.getDate());
    var m               = addZero(data.getMonth() + 1);
    var y               = data.getFullYear();
 
    return y + '/' + m + '/' + d + ' 00:00:00';
}

// Pega a hora da marcação no formato para inserir no DB
function getHour(){
    var data            = new Date();
    var h               = addZero(data.getHours());
    var m               = addZero(data.getMinutes());
 
    return h + ":" + m;
}

// Exibe o horário atual com a contagem de segundos
function displayTime(){
    setInterval(function(){
        var date    = new Date();
        var h       = addZero(date.getHours());
        var m       = addZero(date.getMinutes());
        var s       = addZero(date.getSeconds());
        var fullHour = h + ':' + m + ':' + s;
        document.getElementById('time-point').innerHTML = fullHour;
    }, 500);
}

// Realiza a marcação do ponto de forma assincrona.
function marcarPonto(){
    swal({
        title: "Atenção !",
        text:   "Deseja realmente marcar o ponto?",
        icon: "info",
        buttons: true,
        dangerMode: true
    }).then((val) => {
        if(val){
            // Declaração das variaveis
            var fullDay         = getData();
            var fullHour        = getHour();

            // Realiza a requisição via AJAX
            $.ajax({
                method:'POST',
                url: '../../php/pointMarking.php',
                data: {
                    fullDay:        fullDay,
                    fullHour:       fullHour
                },
                success: function(data){
                    // Atualiza a lista de marcações feitas
                    getHorariosMarcados();
                    if(data == 'true'){
                        swal("Sucesso", "O Ponto foi marcado !", "success");
                    }else{
                        swal("Erro", data, "error");
                    }
                }
            });
        }
    });



}

// Lista as marcações feitas de forma assincrona
function getHorariosMarcados(){
    var diaMarcacao  = getData();
    $.ajax({
        method:'POST',
        url: '../../php/getTimesMark.php',
        data:{diaBusca: diaMarcacao},
        success: function(data){
            document.getElementById('times-mark').innerHTML = data;
        }
    });
}

$(document).ready(function(){
    displayTime();
    getHorariosMarcados();

    $('#btnPoint').click(function(){
        marcarPonto();
    });


});