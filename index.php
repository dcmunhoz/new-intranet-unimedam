<?php
    include('php/_includes.php');
    session_start();

    $conZeus    = new databaseConnect('zeus');
    $linkZeus   = $conZeus->startFirebird();
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <!-- Meta tags importantes -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Folhas de estilo -->
        <link rel="stylesheet" type="text/css" href="assets/css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/main.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/media.css" />

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

        <!-- Titulo -->
        <title>Intranet Unimed Alta Mogiana</title>

        <!-- Icon -->
        <link rel="icon" href="assets/imgs/favicon.png">
    </head>
    <body>
        <noscript>
            <div class="NoScript-Container"> 

                <span class="NoScript-Logo">
                    <img src="assets/imgs/logo-unimed.png" alt="Logo Unimed" title="Logo Unimed">
                </span>

                <span class="NoScript-Alert">
                    <p>Detectamos que seu navegador não esta com o Javascript habilitado ou não é suportado. Por favor, habilite antes de continuar a navegação no site.</p>
                </span>

            </div>  
        </noscript>
        
        <div id="dark-screen"></div>
        <!-- Main Page -->
        <main id="main">
            <!-- Navbar -->
            <nav class="" id="navbar">
                <div class="brand">
                    <a href="#"> <img  id="teste" src="assets/imgs/logo-unimed.png" alt="Logo Unimed Orlândia" title="Logo Unimed Orlândia"> </a>
                </div>
                <div class="menu" id="menu-toggler-show">
                    <i class="fas fa-bars"></i>
                </div>
            </nav>

            <div id="user-menu-collapsed">
                <div class="menu" id="menu-toggler-close">
                    <i class="fas fa-bars"></i>
                </div>
                <?php
                    // Verifica se o usuário esta logado
                    if(isset($_SESSION['user'])){ // Pinel para realizar login
                        include('php/userLogged.php');
                    }else{ // Painel usuário logado
                        include('php/userLogin.php');
                    }
                
                ?>
            </div>

            <!-- Page content -->
            <section id="content">
                <!-- Background intro -->
                <div id="background">
                    <div class="bg-img" id="bg-img">
                    </div>
                    <div class="bg-intro">
                        <span class="bgi-title">Intranet</span>
                        <span class="bgi-desc">Unimed Alta Mogiana</span>
                    </div>
                </div>
                
                <!-- Page body -->
                <div id="page-body" class="container">

                    <!-- User menu -->
                    <div id="user-menu">
                        <?php
                            // Verifica se o usuário esta logado
                            if(isset($_SESSION['user'])){ // Pinel para realizar login
                                include('php/userLogged.php');
                            }else{ // Painel usuário logado
                                include('php/userLogin.php');
                            }
                        
                        ?>
                    </div>

                    <!-- Main content -->
                    <div id="main-content">
                        <div class="main-content-container">

                            <!-- Date day -->
                            <div class="date-day">
                                <?php 
                                    setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                                    date_default_timezone_set('America/Sao_Paulo');
                                    echo strftime('%A, %d de %B de %Y', strtotime('today'));
                                    // $data   = new DateTime();
                                    // $formatter = new IntlDateFormatter('pt_BR', IntlDateFormatter::FULL);
                                    // echo $formatter->format($data);
                                ?>
                            </div>

                            <!-- Brithdays -->
                            <div class="birthdays">
                                <table>
                                    <caption>Aniversáriantes do Mês</caption>
                                    <thead>
                                        <th>Dia</th>
                                        <th>Colaborador</th>
                                    </thead>
                                    <tbody>

                                        <!--    
                                            Exibir os aniversáriantes do mes.
                                         -->
                                        <?php 
                                            // Query de seleção no banco.
                                            $query = "SELECT * FROM funcionario WHERE funativo = 'True' ORDER BY funnascimento desc ;";

                                            // Executa a Query
                                            if($res_query = ibase_query($linkZeus, $query)){

                                                $colaborador = [];
                                                $diaAtual    = (string) date('d');
                                                $mesAtual    = (string) date('m'); 

                                                // Faz um loop até a quantidade final de tuplas.
                                                while($row = ibase_fetch_assoc($res_query)){

                                                    // Captura a o dia e mês de nascimento e o nome.
                                                    $diaNascimentoFun  = explode('-', explode(' ', $row['FUNNASCIMENTO'])[0])[2];
                                                    $mesNascimentoFun  = explode('-', explode(' ', $row['FUNNASCIMENTO'])[0])[1];
                                                    $nomeFun           = $row['FUNNOME'];
                                                    
                                                    // Se o mês de nascimento for igual o mês atual, insere o func no array.
                                                    if($mesNascimentoFun == $mesAtual){
                                                        $colaborador[] = [
                                                            dia  => $diaNascimentoFun,
                                                            nome => $nomeFun
                                                        ];
                                                    }
                                                }

                                                // Ordena pelo dia.
                                                sort($colaborador);

                                                // Exibir na tabela os func.
                                                foreach ($colaborador as $func) {  
                                                    // Se o dia de nascimento for igual o dia do mês, destaca o aniversário     
                                                    if($func['dia'] == $diaAtual){
                                                        $nomeClasse = 'class="aniv"';
                                                    }else{
                                                        $nomeClasse = '';
                                                    }
                                                    
                                                    // Exibe o nome e dia de nascimento na tabela.
                                                    echo "<tr $nomeClasse>\n";
                                                    echo    "<td>".$func['dia']."</td>\n";
                                                    echo    "<td>".utf8_encode($func['nome']);
                                                    // if($nomeClasse != '' && $nomeClasse != null ){
                                                    //     echo    "<div id='setor'>teste</div>";
                                                    // }
                                                    echo    "</td>";
                                                    echo "</tr>\n";
                                                }

                                            }else{
                                                echo "<td colspan='2'>Erro ao executar query</td>";
                                            }
                                        
                                        
                                        ?>
                                        <!-- <tr>
                                            
                                            <td colspan="2">ANIVERSARIOS</td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>

                            <!-- Information -->
                            <div class="information">
                                <span class="title">Nossa História</span>
                                <span class="cit">
                                    "Os princípios do cooperativismo interpretam os valores e adaptam-se ao tempo e ao lugar, fazendo ponte entre a teoria e a prática cooperativista, transformando idéias em ação."
                                </span>

                                <span class="who">
                                    Há 30 anos, um grupo formado pelos médicos Edson Abrahão, Roberto Barbieri Leme da Costa, Akihiro Tukiyama e João Henrique Orsi, entre outros, em uma iniciativa ousada e corajosa, se uniu em um SONHO, com o ideal de associativismo cooperativo, e fundou a Unimed Orlândia (atual Unimed Alta Mogiana).

                                </span>

                                <span class="who">
                                    Hoje, somos parceiros de mais 256 empresas, 59 médicos cooperados, ampla infra-estrutura hospitalar, com presença crescente dos Hospitais Santo Antônio em Orlândia, São Marcos da Sama, Santa Rita e São Geraldo, compondo assim, o mais completo complexo hospitalar regional, com equipes altamente qualificadas e o serviço de pronto atendimento 24 horas, junto ao Pronto Socorro do Hospital Santo Antônio, dotado de ambulância para remoção em casos de urgência.
                                </span>
                                <span class="mvv-title">
                                    Missão
                                </span>
                                <span class="mvv-content">
                                    Promover e cuidar da saúde, com qualidade e segurança, incentivando e valorizando o trabalho médico, superando expectativas de beneficiários, empresas, colaboradores e cooperados, garantindo sustentabilidade.
                                </span>
                                <span class="mvv-title">
                                    Visão
                                </span>

                                <span class="mvv-content">
                                    Liderança de mercado consolidada na sua área de abrangência.
                                </span>

                                <span class="mvv-title">
                                    Valores
                                </span>

                                <span class="mvv-content">
                                    <ul>
                                        <li>Valorização da marca;</li>
                                        <li>Profissionalização administrativa;</li>
                                        <li>Transparência;</li>
                                        <li>Modernização;</li>
                                        <li>Inovação.</li>
                                    </ul>
                                </span>

                            </div>
                        </div>
                    </div>

                </div>
            </section>

        </main>

        <!-- Scripts -->
        <script src="assets/js/jquery.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="assets/js/script.js"></script>
    </body>
</html>