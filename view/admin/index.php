<?php
    // Includes
    include_once('../../php/_includes.php');
    
    // Inicia variavel de sessão
    session_start();


    // Verifica a sessão
    hasSession();

    // Verifica se o usuário tem permissão para acessar o painel de admin
    if($_SESSION['user_type'] != 'A'){
        Header('Location: ../../?errn=1');
    }

    $myCon  = new databaseConnect('intranet');
    $myLink = $myCon->startMysql(); 
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <!-- Meta tags importantes -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Folhas de estilo -->
        <link rel="stylesheet" type="text/css" href="../../assets/css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="../../assets/css/style.css" />
        <link rel="stylesheet" type="text/css" href="../../assets/css/sec.css" />
        <link rel="stylesheet" type="text/css" href="../../assets/css/media.css" />

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

        <!-- Titulo -->
        <title>Intranet Unimed Alta Mogiana</title>

        <!-- Icon -->
        <link rel="icon" href="../../assets/imgs/favicon.png">       

        <!-- Scripts -->
        <script src="../../assets/js/jquery.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="../../assets/js/script.js"></script>
        <script src="../../assets/js/scAdm.js"></script>   
    </head>
    <body>
        <noscript>
            <div class="NoScript-Container"> 

                <span class="NoScript-Logo">
                    <img src="assets/imgs/logo-unimed.png" alt="Logo Unimed" title="Logo Unimed">
                </span>

                <span class="NoScript-Alert">
                    <p>Detectamos que seu navegador não esta com o Javascript habilitado ou não é suportado. Por favor, habilite antes de continuar a navegação no site.</p>
                </span>

            </div>  
        </noscript>
        
        <!-- Main Page -->
        <main id="main">
            <!-- Navbar -->
            <nav class="second-nav">
                <div class="brand">
                    <a href="../../"> <img src="../../assets/imgs/logo-unimed.png" alt="Logo Unimed Orlândia" title="Logo Unimed Orlândia"> </a>
                </div>
                <div class="navbar-user">
                    Logado como<span> <?php echo $_SESSION['user']; ?></span>
                </div>
            </nav>

            <!-- Page content -->
            <section id="content">
                
                <!-- Page body -->
                <div id="page-body" class="container">
                    <div class="box-content" id="adm-panel">
                        <header>
                            <div class="title">
                                <h3>Painel administrativo de usuários</h3>
                                <span>
                                    Pagina para edição dos usuários da Intranet
                                </span>
                            </div>
                        </header>
                        <div class="adm-content" >
                            <table>
                                <caption>
                                    <button type='button' id="btnUser" name="btnUser" class="btn">Novo</button>
                                    <label for="chk-filter">
                                        <input type="checkbox" name="chk-filter" id="chk-filter" checked> Somente Ativos
                                    </label>
                                </caption>
                                <thead>
                                    <tr>
                                        <th>Usuário</th>
                                        <th>Nome</th>
                                        <th>Status</th>
                                        <th>Tipo</th>
                                        <th>Permite Virtual</th>
                                        <th>Opções</th>
                                    </tr>
                                </thead>
                                <tbody id="users-list">

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="box-content" id="adm-user">
                        <header>
                            <div class="title">
                                <h3>Gerenciar Usuário</h3>
                            </div>
                        </header>
                        <div class="user-content">
                            <form class="form-user">
                                <div class="form-group">
                                    <input type="text" name="loginUser" id="loginUser" placeholder="Usuário">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="nameUser" id="nameUser" placeholder="Nome">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="pisUser" id="pisUser" placeholder="PIS">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="pwdUser" id="pwdUser" placeholder="Senha">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="cpwdUser" id="cpwdUser" placeholder="Confirmar Senha">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="emailUser" id="emailUser" placeholder="E-Mail">
                                </div>
                                <div class="form-group">
                                    <select name="slTypeUser" id="slTypeUser">
                                        <option value="U">Usuário</option>
                                        <option value="A">Admin</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="chkPermiteVirtual">
                                        <input type="checkbox" name="chkPermiteVirtual" id="chkPermiteVirtual"> Permite Virtual
                                    </label>
                                </div>
                            </form>
                        </div>
                        <footer class="user-buttons">
                            <button type="button" name="btnFinalizar" id="btnFinalizar">Finalizar</button>
                            <button type="button" name="btnCancelar"  id="btnCancelar" >Cancelar</button>
                        </footer>
                    </div>
                </div>

            </section>

        </main>         

    </body>
</html>