<?php
    include('../../php/_includes.php');
    session_start();
    
    // Verifica se o usuário esta logado para acessar esta pagina
    hasSession();

    $conZeus  = new databaseConnect('zeus');
    $linkZeus = $conZeus->startFirebird();

?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <!-- Meta tags importantes -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Folhas de estilo -->
        <link rel="stylesheet" type="text/css" href="../../assets/css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="../../assets/css/style.css" />
        <link rel="stylesheet" type="text/css" href="../../assets/css/sec.css" />
        <link rel="stylesheet" type="text/css" href="../../assets/css/media.css" />

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

        <!-- Titulo -->
        <title>Intranet Unimed Alta Mogiana</title>

        <!-- Icon -->
        <link rel="icon" href="../../assets/imgs/favicon.png">       

        <!-- Scripts -->
        <script src="../../assets/js/jquery.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="../../assets/js/script.js"></script>
        <script src="../../assets/js/scPoint.js"></script>   
    </head>
    <body>
        <noscript>
            <div class="NoScript-Container"> 

                <span class="NoScript-Logo">
                    <img src="assets/imgs/logo-unimed.png" alt="Logo Unimed" title="Logo Unimed">
                </span>

                <span class="NoScript-Alert">
                    <p>Detectamos que seu navegador não esta com o Javascript habilitado ou não é suportado. Por favor, habilite antes de continuar a navegação no site.</p>
                </span>

            </div>  
        </noscript>
        
        <!-- Main Page -->
        <main id="main">
            <!-- Navbar -->
            <nav class="second-nav">
                <div class="brand">
                    <a href="../../"> <img src="../../assets/imgs/logo-unimed.png" alt="Logo Unimed Orlândia" title="Logo Unimed Orlândia"> </a>
                </div>
                <div class="navbar-user">
                    Logado como<span> <?php echo $_SESSION['user']; ?></span>
                </div>
            </nav>

            <!-- Page content -->
            <section id="content">
                
                <!-- Page body -->
                <div id="page-body" class="container">
                    <div id="point-box" class="box-content">
                        <header>
                            <div class="title">
                                <h3>
                                    Sistema Alternativo de Marcação de Ponto
                                </h3>
                                <span>
                                    Esta página deve ser utilizada somente quando houver algum problema
                                    nos relógios de ponto ou outras falhas.
                                </span>
                            </div>
                            <div class="warning-box">
                                <span class="warning">Atenção: </span> <span class="warning-msg">Você está identificado com o endereço de IP <span class="warning-ip"><?php echo $_SERVER['REMOTE_ADDR']; ?></span>. Essa e outras informações serão gravadas para sabermos se o ponto não está sendo marcado de outros locais que não seja na empresa.</span>
                            </div>
                            <!-- <div class="inst-box">
                                <span class="instruction">Instrução: </span> <span class="instruct-msg">Para marcar o ponto, selecione seu nome na caixa abaixo, apos isso, clique no botão 'Marcar Ponto'.</span>
                            </div> -->
                        </header>
                        <div class="point-content">
                            <div class="pis-num">
                                Seu pis: <span id="pis-num-user"><?php echo $_SESSION['pisnum'];?></span>
                            </div>
                            <div class="time-select">
                                <aside class="marks">
                                    <table>
                                        <caption id="time-point">00:00:00</caption>
                                        <thead>
                                            <tr>
                                                <th>Suas Marcações</th>
                                            </tr>
                                        </thead>
                                        <tbody id="times-mark">
                                            <!-- <tr>
                                                <td>00:00</td>
                                            </tr>
                                            <tr>
                                                <td>00:00</td>
                                            </tr>
                                            <tr>
                                                <td>00:00</td>
                                            </tr>
                                            <tr>
                                                <td>00:00</td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </aside>
                                <div class="func-selection">
                                    <!-- <input type="text" list="dlFunc">
                                    <datalist id="dlFunc" name="dlFunc">
                                    
                                    </datalist> -->
                                    
                                    <select name="slFunc" id="slFunc" disabled>
                                        <?php
                                            $pisFunLogged = $_SESSION['pisnum'];
                                            $query = "SELECT funnome, funpis FROM funcionario WHERE funativo = 'True' AND funpis = '$pisFunLogged';";
                                            if($rq = ibase_query($linkZeus, $query)){
                                                while($row = ibase_fetch_assoc($rq)){
                                                    $funnome        = utf8_encode($row['FUNNOME']);
                                                    $funpis         = $row['FUNPIS'];
                                                    echo "<option value='$funpis'> $funpis - $funnome  </option>";
                                                }
                                            }else{
                                                echo 'Erro: ' . ibase_errmsg($linkZeus);
                                            }
                                        ?>  
                                    </select>
                                    <button class="btn btn-blue" type="button" name="btnPoint" id="btnPoint">Marcar Ponto</button>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

        </main>         

    </body>
</html>